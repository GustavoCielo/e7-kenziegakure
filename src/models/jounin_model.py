# Cole seu código aqui
from .ninja_model import Ninja


class Jounin(Ninja):
    ninja_level: str = "Jounin"

    def __init__(
        self,
        name: str,
        clan: str,
        village: str,
        proficiency: dict = {
            "taijutsu": int,
            "ninjutsu": int,
            "genjutsu": int
        },
    ):
        super().__init__(name, clan, village, ninja_level=self.ninja_level)
        self.proficiency = proficiency
        self.is_in_mission = False

    def list_best_proficiency(self):
        # j = 0
        # i = ""
        # for (key, value) in self.proficiency.items():
        #     if value > j:
        #         j = value
        #         i = key
        best_proficiency = max(
            self.proficiency,
            key=lambda x: self.proficiency[x]
        )
        return f'{self.name} demonstra maior proficiência em {best_proficiency}'

    def start_mission(self):
        if self.is_in_mission:
            return f'O ninja {self.name} {self.clan} já está em uma missão'
        else:
            self.is_in_mission = True
            return f'O ninja {self.name} {self.clan} saiu em missão'

    def return_from_mission(self):
        if not self.is_in_mission:
            return f'O ninja {self.name} {self.clan} não está em nenhuma missão no momento'
        else:
            self.is_in_mission = False
            return f'O ninja {self.name} {self.clan} retornou em segurança da missão'


kakashi_proficiency = {'taijutsu': 7, 'ninjutsu': 8, 'genjutsu': 4}
kakashi = Jounin('Kakashi', 'Hatake', 'Konoha', kakashi_proficiency)

print(kakashi.__dict__)
