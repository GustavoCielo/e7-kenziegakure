# Cole seu código aqui
class Jutsu:
    jutsu_ranks = ('D', 'C', 'B', 'A', 'S',)

    def __init__(
        self, jutsu_name: str,
        jutsu_type: str,
        jutsu_level: str,
        jutsu_damage: int,
        chakra_spend: int
    ):
        self.jutsu_name = jutsu_name
        self.jutsu_type = jutsu_type
        self.jutsu_level = jutsu_level
        self.jutsu_damage = jutsu_damage
        self.chakra_spend = chakra_spend

        if self.chakra_spend <= 0:
            self.chakra_spend = 100
        else:
            self.chakra_spend = chakra_spend

        try:
            index = self.jutsu_ranks.index(jutsu_level.upper())
            self.jutsu_level = self.jutsu_ranks[index]
        except ValueError:
            self.jutsu_level = "Unranked"
        """index quando nao encontra retorna ValueError"""

        # if self.jutsu_level.upper() in self.jutsu_ranks:
        #     self.jutsu_level = self.jutsu_level.upper()
        # else:
        #     self.jutsu_level = "Unranked"
        """if else com método in"""
        # self.jutsu_level = "Unranked" if self.jutsu_level.upper() not in self.jutsu_ranks else jutsu_level.upper()
        """ternário"""
